#!/usr/bin/env ruby

class Entry

  attr_accessor :counter, :id, :group, :author, :created_at, :text, :total_left

  def initialize(group, id, author, text, created_at)
    @id = id
    @author = author
    @text = text
    @counter = text_to_counter()
    @group = group
    @created_at = created_at
  end

  def text_to_counter
    if(@text.to_s.match(/^\-\d+/) or @text.to_s.match(/^\d+/))
      tmp = /\d+/.match(@text.to_s)

      tmp = 0 if(tmp.nil?)

      @counter = tmp[0].to_i

      if(@text.to_s[0] == '-')
        @counter = -(@counter.to_i)
      end

    else
      @counter = 0
    end

    return @counter
  end

  def is_valid
    if (@counter < @max_in_set and @counter > @min_in_set)
      return 0

    elsif (@counter >= @max_in_set and @moderators.include?(@author))
      return 4

    elsif (@counter > @max_in_set)
      @counter = 0
      return 1 # inform about errors, this will trigger sending error message for this entry

    elsif (@counter < @min_in_set and @moderators.include?(@author))
      return 2 #if counter is less than 0, this will **add** value to @total_left instead subtracting

    elsif (@counter < @max_in_set)
      @counter = 0
      return 3

    end
  end

  def error_msg
    return "@#{@author}: Nie możesz za jednym razem dodać wartości większej niż #{@max_in_set - 1}"
  end

  def as_text
    puts @author + " made " + @counter.to_s + " in https://strimoid.pl/e/" + @id
  end

  def left_after
    return @total_left - @counter
  end

  def set_total_left(left)
    @total_left = left
  end

end
