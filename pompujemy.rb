#!/usr/bin/env ruby

require_relative 'entry.rb'

class Pompujemy < Entry

  def initialize(group, id, author, text, created_at)
    @moderators = %w{
                  akerro
                  duxet
                  Kuraito
                  }
    @min_in_set = 0
    @max_in_set = 1001

    @id = id
    @author = author
    @text = text
    @counter = text_to_counter()
    @group = 'Pompujemy'
    @created_at = created_at
  end

  def as_text
    puts @author + ' made ' + @counter.to_s + " from #{@total_left} in https://strimoid.pl/e/" + @id + " we have #{left_after()} to do"
  end

  def response
    result = is_valid()

    if (result == 0)
      response_msg = "#{@total_left} - #{@counter} = #{@total_left - @counter}\n"
      response_msg += '**Chuck Norris jest z Was dumny!**' if (left_after() < 0)
    elsif (result == 1)
      response_msg = error_msg()
    elsif (result == 2)
      response_msg = "@#{@author}: Cofnięto zegar!\n"
      response_msg += "Pozostało #{left_after()} km"
    elsif (result == 4)
      response_msg = "#{@total_left} - #{@counter} = #{@total_left - @counter}\n"
    end

    return response_msg
  end

end
