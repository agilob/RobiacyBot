require 'test/unit'
require_relative 'pompujemy.rb'
require_relative 'rowerowyrownik.rb'

class MyTest < Test::Unit::TestCase

  def test_reversing_clock_on_positive
    entry = Pompujemy.new('pompujemy', 'init_id', 'akerro', '100 blablabla', "11.02.2015")
    entry.set_total_left(1000)
    assert(entry.counter == 100)
  end

  def test_reversing_clock_on_negative
    entry = Pompujemy.new('pompujemy', 'init_id', 'akerro', '-100 blablabla', "11.02.2015")
    entry.set_total_left(1000)
    assert(entry.counter == -100)
  end

  def test_is_moderator_is_valid
    entry = Pompujemy.new('pompujemy', 'init_id', 'akerro', '-100 blablabla', "11.02.2015")
    entry.set_total_left(1000)
    assert(entry.is_valid == 2)
  end

  def test_is_moderator_reversing_clock
    entry = Pompujemy.new('pompujemy', 'init_id', 'akerro', '-100 blablabla', "11.02.2015")
    entry.set_total_left(1000)
    entry.response() #performs validation
    assert(entry.left_after == 1100)
  end

  def test_is_not_moderator_is_valid
    entry = Pompujemy.new('pompujemy', 'init_id', 'zryty_beret', '-100 blablabla', "11.02.2015")
    entry.set_total_left(1000)
    assert(entry.is_valid == 3)
  end

  def test_is_not_moderator_left_after
    entry = Pompujemy.new('pompujemy', 'init_id', 'zryty_beret', '-100 blablabla', "11.02.2015")
    entry.set_total_left(1000)
    entry.response() #performs validation
    assert(entry.left_after == 1000)
  end

  def test_starts_with_minus_letter
    entry = Pompujemy.new('pompujemy', 'init_id', 'akerro', '-e0 blablabla', "11.02.2015")
    entry.set_total_left(1000)
    entry.response() #performs validation
    assert(entry.counter == 0)
  end

  def test_starts_with_minus_number
    entry = Pompujemy.new('pompujemy', 'init_id', 'zryty_beret', '-100 blablabla', "11.02.2015")
    entry.set_total_left(1000)
    entry.response() #performs validation
    assert(entry.counter == 0)
  end

  def test_starts_with_minus_number_as_moderator
    entry = Pompujemy.new('pompujemy', 'init_id', 'akerro', '-100 blablabla', "11.02.2015")
    entry.set_total_left(1000)
    entry.response() #performs validation
    assert(entry.counter == -100)
  end

  def test_starts_with_letters_minus_as_moderator
    entry = Pompujemy.new('pompujemy', 'init_id', 'akerro', 'dasdadad -100 blablabla', "11.02.2015")
    entry.set_total_left(1000)
    entry.response() #performs validation
    assert(entry.counter == 0)
  end

  def test_starts_with_letters_positive_as_moderator
    entry = Pompujemy.new('pompujemy', 'init_id', 'akerro', 'dasdadad -100 blablabla', "11.02.2015")
    entry.set_total_left(1000)
    entry.response() #performs validation
    assert(entry.counter == 0)
  end

  def test_starts_with_letters_negative
    entry = Pompujemy.new('pompujemy', 'init_id', 'akerro', 'dasdadad -100 blablabla', "11.02.2015")
    entry.set_total_left(1000)
    entry.response() #performs validation
    assert(entry.counter == 0)
  end

  def test_starts_with_letters_positive
    entry = Pompujemy.new('pompujemy', 'init_id', 'akerro', 'dasdadad -100 blablabla', "11.02.2015")
    entry.set_total_left(1000)
    entry.response() #performs validation
    assert(entry.counter == 0)
  end

  def test_too_big_value
    entry = Pompujemy.new('pompujemy', 'init_id', 'not_moderator', '10000', "11.02.2015")
    entry.set_total_left(1000)
    assert(entry.response.eql?(entry.error_msg))
  end

  def test_zero_value
    entry = Pompujemy.new('pompujemy', 'init_id', 'Kuraito', '0', "11.02.2015")
    entry.set_total_left(1000)
    assert(entry.counter == 0)
  end

  def test_moderator
    entry = Pompujemy.new('pompujemy', 'init_id', 'Kuraito', '-100', "11.02.2015")
    entry.set_total_left(1000)
    entry.response() #performs validation
    assert(entry.counter == -100)
  end

  def test_add_milion_as_moderator
    entry = RowerowyRownik.new('RowerowyRownik', 'init_id', 'zskk', '999999', "11.02.2015")
    entry.set_total_left(1000000)
    puts entry.response() #performs validation
    assert(entry.is_valid == 4)
  end

end
