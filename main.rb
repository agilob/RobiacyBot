#!/usr/bin/env ruby

require 'rubygems'
require 'json'
require 'sqlite3'
require 'httparty'
require 'sequel'

require_relative 'token.rb'
require_relative 'entry.rb'
require_relative 'pompujemy.rb'
require_relative 'rowerowyrownik.rb'

@groups = %w{
Pompujemy
RowerowyRownik
}

def main
  @db = Sequel.connect('sqlite://robiacybot.sqlite3')
  #create_database()
  #init_group('RowerowyRownik')

  @groups.each { |group|
    entries = get_entries_from_group(group)

    entries.each { |entry|
      total_left = get_last_total_left_for_group(group)
      entry.set_total_left(total_left)

      if(doesnt_contain(entry.id)) #already in the database
        save_entry(entry) if(post(entry) == 200)
        sleep(2) #sleep 2 second to avoid flooding
      else
        puts "Mam wpis o id: #{entry.id}"
      end

    } # entry each
  } # group each

end

def init_group(group)
  e = Kernel.const_get(group).new(
      group,
      'init_id',
      'init_author',
      'init_text',
      DateTime.now
  )
  e.set_total_left(40076)
  e.as_text
  save_entry(e)
end

def save_entry(e)
  entries = @db[:entries]
  entries.insert(
      :group => e.group,
      :entry_id => e.id,
      :author => e.author,
      :text => e.text,
      :counter => e.counter,
      :total_left => e.left_after,
      :created_at => e.created_at
  )
end

def doesnt_contain(id)
  checker = @db.from(:entries)
  contains = checker.where(:entry_id => id.to_s)
  contains.count == 0
end

def get_last_total_left_for_group(group)
  selector = @db.from(:entries)
  last_entry = selector.where(:group => group)
  last_entry.order(:id).last[:total_left]
end

def post(e)
  @header = {'Authorization' =>  "Bearer #{@token}",
             'Content-Type' => 'application/json'}

  data = {:text => e.response(), :group => e.group}

  begin
    response = HTTParty.post("https://strimoid.pl/api/v1/entries/#{e.id}/replies",
                             :body => data.to_json,
                             :headers => @header)
    return response.code

  rescue Net::ReadTimeout
  end
  return 0
end

def get_entries_from_group(group)
  response = HTTParty.get("https://strimoid.pl/api/v1/entries?group=#{group}&page=1")
  result = JSON.parse(response.body)
  es = Array[]

  result['data'].reverse.each { |entry|
    entry_id = entry['_id']
    author = entry['user']['name']
    text = entry['text_source']
    created_at = entry['created_at']

    # converts name of a group to a constructor of proper class
    # 'pompujemy' => Popmpujemy.new
    es << Kernel.const_get(group).new(group, entry_id, author, text, created_at)
  }
  return es

end

def create_database
  begin #try catch for table already exists
  @db.create_table :entries do
    primary_key :id
    String :group
    String :entry_id
    String :author
    String :text, :size => 2500
    int :counter
    int :total_left
    DateTime :created_at, :default => DateTime.now
  end
    puts 'database created!'

  rescue SQLite3::SQLException, Sequel::DatabaseError
    puts 'table already exists!'
  end
end

main
