#!/usr/bin/env ruby

require_relative 'entry.rb'

class RowerowyRownik < Entry


  def initialize(group, id, author, text, created_at)
    @moderators = %w{
                  akerro
                  duxet
                  zskk
                  szarak
                  }

    @min_in_set = 0
    @max_in_set = 151

    @id = id
    @author = author
    @text = text
    @counter = text_to_counter()
    @group = 'RowerowyRownik'
    @created_at = created_at
  end

  def as_text
    puts @author + ' cycled ' + @counter.to_s + "kms in https://strimoid.pl/e/#{@id}"
  end

  def response
    result = is_valid()

    if (result == 0)
      response_msg = "#{@total_left} - #{@counter} = #{@total_left - @counter}\n"
      response_msg += 'Gratulacje właśnie dzięki wspólnym siłom orkążyliście Ziemię!' if (left_after() < 0)
    elsif (result == 1)
      response_msg = error_msg()
    elsif (result == 2)
      response_msg = "@#{@author}: Cofnięto zegar!\n"
      response_msg += "Pozostało #{left_after()} km"
    elsif (result == 4)
      response_msg = "#{@total_left} - #{@counter} = #{@total_left - @counter}\n"
    end

    return response_msg
  end

end
